# Authentication system `authing`

## Database
Creates tables and API routes for diferent account registration, authentication and management procedures.

Tables:
- `${cfg.table_prefix}_submitted_emails` - emails to be verified
- `${cfg.table_prefix}_accounts`
- `${cfg.table_prefix}_sessions`
- `${cfg.table_prefix}_recovery` - account recovery codes
- `${cfg.table_prefix}_session_tabs` - security related tab separation

The default value of `cfg.table_prefix` is `flyauth`.

## Routes
Listens on these web routes for signup and authentication requests:

- `${cfg.web_route_path}_flyauth/public-key` - obtain a public key to assymetricly encrypt credentials
- `${cfg.web_route_path}_flyauth/submit-email` - submit emails to be verified
- `${cfg.web_route_path}_flyauth/signup` - provide email verification code with credentials to create an account
- `${cfg.web_route_path}_flyauth/init-session` - initialize session before authentication in order to encrypt credentials
- `${cfg.web_route_path}_flyauth/signin` - authenticate and finalize session creation
- `${cfg.web_route_path}_flyauth/last-ots` - send your last *one time secret* to get an array of new ones
- `${cfg.web_route_path}_flyauth/signout` - destroy session


## Middlewares
Use these middleware functions for protected/forwarding routes:

- `flyauth.default_middlewares` - sets default request properties, parses cookies and generates a request fingerprint
- `flyauth.forward_credentials_mwfn` - forwards credentials to an unprotected path to maintain session
- `flyauth.authorize_mwfn` - authorization for protected paths

## Font-end
You can create your own scripts for authenticating but it's highly recommended to use `lib/flyauth.ui` twig, because API documentation is still very scarse. This twig does most of the work for you.
