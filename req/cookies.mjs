

export default class RequestCookies {
  static middleware(req, res, next) {
    const cookies = {};
    const cstrs = req.headers.cookie ? req.headers.cookie.split(";") : [];
    for (let cstr of cstrs) {
      const key_val = cstr.trim().split("=");
      cookies[key_val[0]] = key_val[1];
    }

    req.cookies = cookies;
    next();
  }
}
