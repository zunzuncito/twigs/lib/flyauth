
import url from 'url'

import AES from 'zunzun/flypto/aes.mjs'

import qs from 'querystring'

export default class RequestParameters {
  static get_enc_keys(req, prefix) {
    const req_aes = new AES(req.aes_key)

    const enc_key_cookie = req.cookies[prefix+"flyauth_enc_key"];
    if (enc_key_cookie && req.fingerprint_weak) {
      const reqfp_aes = new AES(AES.derive_key(req.fingerprint_weak, req.salt, 1000));
      return JSON.parse(
        reqfp_aes.decrypt(
          req_aes.decrypt(
            decodeURIComponent(enc_key_cookie)
          )
        )
      );
    } else {
      return enc_key_cookie;
    }
  }

  static parse_aes(req, prefix) {
    let params = req.params;
    try {
      const enc_keys = this.get_enc_keys(req, prefix);
      if (enc_keys) {
        const dec_aes = new AES(enc_keys.iaes);
        const enc_aes = new AES(enc_keys.oaes);

        if (params.authorized) {
          params = JSON.parse(dec_aes.decrypt(decodeURIComponent(params.authorized)));
        } else {
          params = qs.parse(req.headers["authorization"]);
          if (params.authorized) {
            params = JSON.parse(dec_aes.decrypt(decodeURIComponent(params.authorized)));
          }
        }

        req.oaes = params.oaes = enc_aes;
      }
    } catch (e) {
      console.error(e.stack);
      params = undefined;
    }
    return params;
  }

  static parse_rsa(key_mg, req) {
    let params = req.params;
    try {
      if (params.authorized) {
        params = JSON.parse(key_mg.decrypt(params.authorized));
      }
    } catch (e) {
      console.error(e.stack)
      params = undefined;   
   enc_aes }
    return params;
  }
}
