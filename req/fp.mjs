
import AES from 'zunzun/flypto/aes.mjs'

import url from 'url';

import URLUtil from 'zunzun/flyutil/url.mjs'

export default class RequestFingerprint {

  static set(req) {
    const alang = req.headers['accept-language'] ?
      req.headers['accept-language'].replace(/;q=\d.\d/, '') :
      "not-specified";
    const unique_values = [
      req.headers['user-agent'],
      req.headers['accept-encoding'],
      alang,
      req.headers['x-forwarded-for'] || req.socket.remoteAddress,
      req.headers['client-key']
    ];
    req.fingerprint_weak = RequestFingerprint.parse(unique_values).toString();
    return unique_values;
  }

  static parse(unique_values) {

    let fingerprint = Number.MAX_SAFE_INTEGER / 2; // A lucky number as fingerprint base ;)
    function fp_add_val(uval) {
      if (typeof uval !== 'number') uval = 0;
      if (uval <= 128) uval += Math.pow(uval, 3+Math.round(uval/128*3));
      uval = uval % 2 === 0 ? uval : -uval;
      fingerprint += uval;
    }

    for (let v = 0; v < unique_values.length; v++) {
      if (typeof unique_values[v] === 'string') {
        for (let c = 0; c < unique_values[v].length; c++) {
          fp_add_val(unique_values[v].charCodeAt(c));
        }
      } else {
        fp_add_val(unique_values[v]);
      }
    }

    return fingerprint;
  }

  static middleware(req, res, next) {

    let params = req.params;
    if (req.method === "GET") {
      params = url.parse(req.url, true).query;
    }

    if (params.cks && req.cookies[req.flyauth_prefix+'flyauth_client_key']) {
      const unique_values = RequestFingerprint.set(req);
      const req_aes = new AES(req.aes_key);
      const reqfp_weak_aes = new AES(AES.derive_key(req.fingerprint_weak, req.salt, 1000));
      const client_key = reqfp_weak_aes.decrypt(
        req_aes.decrypt(
          decodeURIComponent(req.cookies[req.flyauth_prefix+'flyauth_client_key'])
        )
      );
      
      const sess_cks = AES.key_sum(client_key);
      const request_cks = parseInt(params.cks);

      if (sess_cks === request_cks) {
        unique_values.push(client_key);
        req.fingerprint = RequestFingerprint.parse(unique_values).toString();
      } else {
        console.error(new Error(
          `Client checksum mismatch! ${sess_cks} !=== ${request_cks} (session !=== request)`
        ));
      }
      req.cks = params.cks;
    } else if (req.url.endsWith("/flyauth/init-session")) {
      const unique_values = RequestFingerprint.set(req);

      const new_client_key = (new AES()).key;
      unique_values.push(new_client_key);
      req.fingerprint = RequestFingerprint.parse(unique_values).toString();

      const reqfp_weak_aes = new AES(AES.derive_key(req.fingerprint_weak, req.salt, 1000));

      req.new_client_key = reqfp_weak_aes.encrypt(new_client_key);

      req.new_cks = AES.key_sum(new_client_key);
    }

    next();
  }
}
