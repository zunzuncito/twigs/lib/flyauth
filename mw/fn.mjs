

import path from 'path'

import bcrypt from 'bcrypt'

import AES from 'zunzun/flypto/aes.mjs'

import Session from '../session.mjs'
import ReqParams from '../req/params.mjs'
import IncludeScripts from '../include/index.mjs'


export default class MiddlewareFunction {
  static async construct() {
    try {
      const _this = new MiddlewareFunction();

      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }

  static async authorize(req, res, next, params, flyauth) {
    if (
      typeof params === "object" &&
      typeof params.fingerprint === "string" &&
      typeof params.tab_id === "number" &&
      typeof params.csrf_secret === "string" &&
      params.csrf_secret.length === 32
    ) {
      const target_accounts_table = flyauth.table("accounts");
      const target_session_table = flyauth.table("sessions");
      const target_tabs_table =  flyauth.table("session_tabs");

      const session_id = (await target_tabs_table.select("session_id", "id = $1", [ params.tab_id ]))[0].session_id;

      const session = (await target_session_table.select(["key", "xss_secret", "fingerprint", "cookie_secret", "userid"], "id = $1", [session_id]))[0];
      
      const cookies = req.cookies;

      if (
        session &&
        bcrypt.compareSync(params.fingerprint, session.fingerprint)
      ) {
        const reqfp_aes = new AES(AES.derive_key(req.fingerprint, req.salt, 1000));
        const key_aes = new AES(reqfp_aes.decrypt(session.key));
        const cookie_secret = key_aes.decrypt(session.cookie_secret);
        const cookie_aes = new AES(cookie_secret);
        const xss_secret = cookie_aes.decrypt(
          flyauth.key_mg.decrypt(
            decodeURIComponent(cookies[flyauth.cfg.auth_prefix+"flyauth_anti_xss"]), "aes"
          )
        );

        const gen_ots = req.url_path.endsWith(".html") || req.url_path === "/" || req.url_path === flyauth.cfg.web_route_path+'/flyauth/last-ots' ? 9 : 0;
        const new_secrets = await Session.update(
          target_session_table,
          target_tabs_table,
          [],
          [],
          session_id,
          params.tab_id,
          params.csrf_secret,
          xss_secret,
          gen_ots,
          req.url_path.endsWith(".html"),
          req.params.new_tab
        );
        if (new_secrets) {
          const user = (await target_accounts_table.select(["username", "privileges"], "id = $1", [session.userid]))[0];
          req.session = {
            id: session_id,
            new_csrf_secret: new_secrets.csrf,
            new_xss_secret: new_secrets.xss,
            username: user.username,
            userid: session.userid
          };

          if (req.web_ctx) {
            const enc_keys = ReqParams.get_enc_keys(req, flyauth.cfg.auth_prefix);
            const enc_creds = key_aes.encrypt(JSON.stringify({
              session_id: session_id,
              tab_id: new_secrets.tab_id,
              csrf_secrets: new_secrets.csrf,
              oaes: enc_keys.iaes,
              iaes: enc_keys.oaes,
              cks: req.cks
            }));

            req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_head_script = IncludeScripts.head(
              flyauth.cfg,
              flyauth.key_mg.latest.rsa.public_key,
              enc_creds
            );
            req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_body_script = IncludeScripts.body();
            req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_session = true;
          }

          const enc_csrf_only = new_secrets.csrf ? key_aes.encrypt(JSON.stringify(new_secrets.csrf)): undefined;
          req.flyauth = {
            ncsrfs: enc_csrf_only,
            params: params.data,
            privileges: user.privileges
          };

          next();
        } else {
          res.writeHead(401, {"Content-Type": "text/plain"});
          res.end("Unauthorized");
        }
      } else {
        res.writeHead(401, {"Content-Type": "text/plain"});
        res.end("Unauthorized");
      }

    } else {
      if (req.headers.accept && (req.headers.accept.includes('application/xhtml+xml') ||
        req.headers.accept.includes('application/xml'))) {

        console.log("UNAUTH REDIRECT", params);

        res.writeHead(302, {
          'Location': flyauth.cfg.signin_path
          //add other headers here...
        });
        res.end("Forbidden");
      } else {
        res.writeHead(403, {"Content-Type": "text/plain"});
        res.end("Forbidden");
      }
    }
  }

  static async forward_credentials(req, res, next, params, flyauth) {
    if (
      typeof params === "object" &&
      params.tab_id
    ) { 
      await this.authorize(req, res, next, params, flyauth);
    } else {
      req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_head_script = IncludeScripts.head(
        flyauth.cfg,
        flyauth.key_mg.latest.rsa.public_key
      );
      req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_body_script = IncludeScripts.body();
      req.web_ctx[flyauth.ctx_group.route_path].ectx.authing_session = false;
      next();
    }
  }
}
