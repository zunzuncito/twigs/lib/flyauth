
import crypto from 'crypto'

import REST_API from './rest/api.mjs'

import KeyMG from './key/mg.mjs'
import SaltMG from './salt/mg.mjs'

import url from 'url'


import mwfn from './mw/fn.mjs'

import ReqCookies from './req/cookies.mjs'
import ReqFp from './req/fp.mjs'
import ReqParams from './req/params.mjs'

export default class FlyAuth {
  static async construct(pg, ctx_group, cfg) {
    try {
      if (typeof cfg !== "object") cfg = {};
      if (!cfg.auth_prefix) cfg.auth_prefix = "";
      if (!cfg.table_prefix) cfg.table_prefix = cfg.auth_prefix+"flyauth";

      const submitted_email_table = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_submitted_emails`, {
        id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
        username: "varchar(256)",
        email: "varchar(256)",
        verification_code: 'varchar(24)',
        verification_code_exp: 'bigint'
      });

      const accounts_table = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_accounts`, {
        id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
        username: "varchar(256)",
        email: 'varchar(60)',
        password: 'varchar(60)',
        privileges: 'text[]',
        api_key: "text"
      });

      const auth_attempts = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_failed_attempts`, {
        userid: `UUID REFERENCES ${cfg.table_prefix}_accounts(id) ON DELETE CASCADE`,
        times: 'smallint',
        ip_addrs: 'text[]',
        captcha: 'varchar(60)'
      });

      const sessions_table = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_sessions`, {
        id: `uuid PRIMARY KEY DEFAULT gen_random_uuid()`,
        userid: `UUID REFERENCES ${cfg.table_prefix}_accounts(id) ON DELETE CASCADE`,
        key: `text`,
        xss_secret: `varchar(60)`,
        cookie_secret: `text`,
        fingerprint: `varchar(60)`,
        src_ip: `varchar(60)`,
        exp: `bigint`
      });

      const recovery_table = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_recovery`, {
        code: 'varchar(32)',
        expiration: 'bigint'
      });

      const session_tabs_table = cfg.skip_tables ? undefined : await pg.table(`${cfg.table_prefix}_session_tabs`, {
        id: 'serial primary key',
        session_id: `UUID REFERENCES ${cfg.table_prefix}_sessions(id) ON DELETE CASCADE`,
        csrf_secrets: `text[]`,
        xss_secret: `varchar(8)`,
        last_used: 'bigint'
      });

      const _this = new FlyAuth(
        ctx_group,
        submitted_email_table,
        accounts_table,
        auth_attempts,
        sessions_table,
        session_tabs_table,
        recovery_table,
        cfg,
        pg
      )

      if (ctx_group) {
        ctx_group.flyauth = _this;

        for (let rctx of ctx_group.contexts) {
          for (let dmw of _this.default_middlewares) {
            rctx.add_middleware(dmw);
          }
          if (
            rctx.lctx &&
            rctx.lctx.flyauth &&
            rctx.lctx.flyauth.authorize
          ) {
            console.log(rctx.dir_path);
            console.debug("ADDING AUTHORIZE MIDDLEWARE");
            rctx.add_middleware(_this.authorize_mwfn);
          } else {
            console.log(rctx.dir_path);
            console.debug("ADDING FORWARD CREDENTIALS MIDDLEWARE");
            rctx.add_middleware(_this.forward_credentials_mwfn);
          }

          await rctx.serve(true);
        }
      }

      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(
    ctx_group,
    submitted_email_table,
    accounts_table,
    auth_attempts,
    sessions_table,
    session_tabs_table,
    recovery_table,
    cfg,
    pg
  ) {
    this.http = ctx_group ? ctx_group.webctx_mg.flyweb.http : undefined;
    if (!cfg.web_route_path) cfg.web_route_path = ""
    if (!cfg.signin_path) cfg.signin_path = "/"
    if (typeof cfg.email_verification == undefined) cfg.email_verification = true

    this.cfg = cfg;
    this.ctx_group = ctx_group;


    this.submitted_email_table = this.submitted_emails_table = submitted_email_table;
    this.accounts_table = accounts_table;
    this.auth_attempts  = this.failed_attempts_table = auth_attempts;
    this.sessions_table = sessions_table;
    this.session_tabs_table = session_tabs_table;
    this.recovery_table = recovery_table;
    this.pg = pg;
    
    if (this.http) {
      this.salt_mg = new SaltMG();
      this.key_mg = new KeyMG(ctx_group, cfg.auth_prefix);

      const _this = this;

      this.default_middlewares = [
        function(req, res, next) {
          req.aes_key = _this.key_mg.latest.aes.key;
          req.salt = _this.salt_mg.latest;
          req.flyauth_prefix = cfg.auth_prefix;
          next()
        },
        ReqCookies.middleware,
        ReqFp.middleware
      ]



      this.forward_credentials_mwfn = async (req, res, next) => {
        try {
          const params = ReqParams.parse_aes(req, cfg.auth_prefix);
          await mwfn.forward_credentials(req, res, next, params, _this);
        } catch (e) {
          console.error(e.stack);
          res.writeHead(500, {"Content-Type": "text/plain"});
          res.end("Internal Server Error");
        }
      }

      this.authorize_mwfn = async (req, res, next) => {
        try {
          const params = ReqParams.parse_aes(req, cfg.auth_prefix);
          req.params = params;
          await mwfn.authorize(req, res, next, params, _this);
        } catch (e) {
          console.error(e.stack);
          res.writeHead(500, {"Content-Type": "text/plain"});
          res.end("Internal Server Error");
        }
      }

      this.rest_api = new REST_API(this);

      setInterval(() => {
        ctx_group.g_tpl_ctx['flyauth_time'] = Date.now();
      }, 1000)


      const clean_up = async () => {
        try {
          const sessions_target_table = await _this.table("sessions");
          const sessions_tabs_target_table = await _this.table("session_tabs");
          const submitted_email_target_table = await _this.table("submitted_emails");
          let exp_sessions = await sessions_target_table.select(["id", "exp"], "exp < $1", [ Date.now() ]);
          for (let exp_sess of exp_sessions) {
            await sessions_tabs_target_table.delete("session_id = $1", [ exp_sess.id ]);
            await sessions_target_table.delete("id = $1", [ exp_sess.id ]);
          }

          await submitted_email_target_table.delete("verification_code_exp < $1", [ Date.now() ]);
        } catch (e) {
          console.error(e.stack);
        }
      }

      setInterval(clean_up, 30 * 60 * 1000); // Every half an hour
    }
  }

  set_mailer(instance) {
    this.mailer = instance;
  }

  table(table_name) {
    return this.flycms ?
      this.flycms.get_model(`${this.cfg.table_prefix}_${table_name}`).table : 
      this[`${table_name}_table`];
  }

}
