import { createCanvas, registerFont } from 'canvas';
import fs from 'fs';

//const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
const colors = [
  '#000000',
  '#FF0000',
  '#FF9900',
  '#00FF00',
  '#00CCCC',
  '#0000FF',
  '#FF00FF'
]
const fonts = [
  'Consolas',
  'Courier New',
  'Roboto Mono',
  'Inconsolata',
  'Fira Code'
]

export default class CAPTCHA {
  static generate(width, height, textLength) {
    // Create a new canvas
    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    // Set background color
    ctx.fillStyle = '#ffffff';
    ctx.fillRect(0, 0, width, height);

    // Generate random text
    let captchaText = '';
    for (let i = 0; i < textLength; i++) {
      captchaText += characters.charAt(Math.floor(Math.random() * characters.length));
    }


    let c_x = 15;

    let last_y = 0;

    for (let c of captchaText) {
      const font = fonts[Math.floor(fonts.length * Math.random())];
      const font_size = 25 + Math.round(Math.random() * 10);
      ctx.font = `${font_size}px ${font}`;
      console.log(ctx.font);
      ctx.fillStyle = colors[Math.floor(colors.length * Math.random())];
      ctx.save();
      let char_y = (height-40)*Math.random()+30;
      let dist_y = char_y - last_y;
      if (dist_y < 0)  dist_y * -1;
      if (dist_y < 20) {
        if (char_y > 40) {
          char_y -= 20;
        } else {
          char_y += 30;
        }
      }
      console.log("char y", char_y);
      ctx.translate(c_x, char_y);
      ctx.rotate(Math.PI / 2 * Math.random() - Math.PI / 4);
      ctx.scale(0.5 * Math.random() + 0.5, 0.5 * Math.random() + 0.5);
      ctx.fillText(c, 0, 0);
      ctx.restore();

      c_x += 30;
      last_y = char_y;
    }



    return {
      text: captchaText,
      buffer: canvas.toBuffer()
    }
  }
}
