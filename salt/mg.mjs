
import bcrypt from 'bcrypt'

export default class SaltManager {
  constructor() {

    this.update();
  }

  update() {
    this.salts = [
      bcrypt.genSaltSync(10)
    ]
    this.latest = this.salts[this.salts.length-1];
  }
}
