import crypto from 'crypto'
import fs from 'fs'

import bcrypt from 'bcrypt'

import EmailUtil from 'zunzun/flyutil/email.mjs'
import AES from 'zunzun/flypto/aes.mjs'

import Session from '../session.mjs'

import ReqParams from '../req/params.mjs'

import nunjucks from 'nunjucks'

import CAPTCHA from '../captcha/index.mjs'

export default class REST_API {
  constructor(flyauth) {
    this.http = flyauth.http;

    this.accounts_table = flyauth.accounts_table;
    this.auth_attempts = flyauth.auth_attempts;
    this.submitted_email_table = flyauth.submitted_email_table;
    this.sessions_table = flyauth.sessions_table;
    this.session_tabs_table = flyauth.session_tabs_table;
    this.recovery_table = flyauth.recovery_table;
    
    this.key_mg = flyauth.key_mg;

    this.submitted_email_expiration = 1000 * 60 * 60;

    this.session_expiration = 1000 * 60 * 60 * 3;

    this.table_prefix = flyauth.cfg.table_prefix;

    this.cfg = flyauth.cfg;


    const _this = this;

    if (flyauth.cfg.email_verification) {
      this.http.put(flyauth.cfg.web_route_path+"/flyauth/submit-email", async (req, res) => {
        try {
          let params = undefined;
          try {
            params = JSON.parse(_this.key_mg.decrypt(req.params.data));
          } catch (e) {
            res.writeHead(422, {"Content-Type": "text/plain"});
            res.end("Unprocessable Entity");
            return
          }

          if (
            typeof params.username === 'string' &&
            params.username.match(/^[A-Za-z0-9_.-]+$/) &&
            params.username.length > 1 &&
            typeof params.email === 'string' &&
            EmailUtil.validate(params.email)
          ) {
            const target_accounts_table = flyauth.table("accounts");
            const target_submitted_emails_table = flyauth.table("submitted_emails");

            let salt_val = ""
            const email64 = Buffer.from(params.email).toString('base64');
            for (let i = 0; i < 3; i++) {
              for (let c = 0; c < 8; c++) {
                let char_index = Math.floor(email64.length / 8 * c)+i;
                if (char_index >= email64.length) char_index -= email64.length;
                salt_val += email64.charAt(char_index);
              }
            }
            salt_val = salt_val.slice(0, -2);
            const email_hash = bcrypt.hashSync(params.email, `$2b$10$${salt_val}`)

            if (
              (await target_accounts_table.select(
                "*", "username = $1",[ params.username ]
              )).length === 0 &&
              (await target_submitted_emails_table.select(
                "*", "username = $1",[ params.username ]
              )).length === 0 &&
              (await target_accounts_table.select(
                "*", "email = $1",[ email_hash ]
              )).length === 0 &&
              (await target_submitted_emails_table.select(
                "*", "email = $1",[ email_hash ]
              )).length === 0
            ) {
              const verification_code = crypto.randomBytes(18).toString('base64');

              const target_table = flyauth.flycms ? await flyauth.flycms.get_model(`${_this.table_prefix}_submitted_emails`) : _this.submitted_email_table;

              const sgnid = (target_table.insert([
                `username`,
                `email`,
                `verification_code`,
                `verification_code_exp`
              ], [`$1`, `$2`, `$3`, `$4`], [
                params.username,
                email_hash,
                verification_code,
                Date.now() + _this.submitted_email_expiration
              ])).id;
              await flyauth.mailer.send_mail(
                flyauth.cfg.email_verification.full_name,
                flyauth.cfg.email_verification.from,
                params.email,
                flyauth.cfg.email_verification.subject,
                nunjucks.renderString(
                  fs.readFileSync(flyauth.cfg.email_verification.text, 'utf8'),
                  { 
                    code: verification_code,
                    url_code: encodeURIComponent(verification_code)
                  }
                ),
                nunjucks.renderString(
                  fs.readFileSync(flyauth.cfg.email_verification.html, 'utf8'),
                  { 
                    code: verification_code,
                    url_code: encodeURIComponent(verification_code)
                  }
                )
              );

              res.writeHead(200, {"Content-Type": "text/plain"});
              res.end("OK");
            } else {
              await _this.delay("signup_conflict");
              res.writeHead(409, {"Content-Type": "text/plain"});
              res.end("Conflict");
            }
          } else {
            res.writeHead(422, {"Content-Type": "text/plain"});
            res.end("Unprocessable Entity");
          }
        } catch (e) {
          console.error(e.stack);
          res.writeHead(500, {"Content-Type": "text/plain"});
          res.end("Internal Server Error");
        }
      });
    }

    this.http.put(flyauth.cfg.web_route_path+"/flyauth/signup", async (req, res) => {
      try {
        let params = undefined;
        try {
          params = JSON.parse(_this.key_mg.decrypt(req.params.data));
        } catch (e) {
          res.writeHead(422, {"Content-Type": "text/plain"});
          res.end("Unprocessable Entity");
          return
        }

        const target_accounts_table = flyauth.table("accounts");
        const target_submitted_emails_table = flyauth.table("submitted_emails");

        if (flyauth.cfg.email_verification) {
          if (params.verification_code) {

            const submitted = await target_submitted_emails_table.select(
              "*", "verification_code = $1", [params.verification_code]
            );

            if (submitted.length > 0) {
              const salt = bcrypt.genSaltSync(10);

              const target_table = flyauth.flycms ? flyauth.flycms.get_model(`${_this.table_prefix}_accounts`) : _this.accounts_table;
              await target_table.insert([
                `username`, `email`, `password`
              ], [`$1`, `$2`, `$3`], [
                submitted[0].username,
                submitted[0].email,
                bcrypt.hashSync(params.password, salt)
              ])

              target_submitted_emails_table.delete("username = $1", [ submitted[0].username ]);

              res.writeHead(200, {"Content-Type": "text/plain"});
              res.end("OK");
            } else {
              res.writeHead(422, {"Content-Type": "text/plain"});
              res.end("Unprocessable Entity");
            }
          } else {
            res.writeHead(422, {"Content-Type": "text/plain"});
            res.end("Unprocessable Entity");
          }

        } else {
          const salt = bcrypt.genSaltSync(10);
          await target_accounts_table.insert([
            `username`, `password`
          ], [`$1`, `$2`], [
            params.username,
            bcrypt.hashSync(params.password, salt)
          ]);
        }


      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });

    this.http.post(flyauth.cfg.web_route_path+"/flyauth/init-session", ...flyauth.default_middlewares, async (req, res) => {
      try {
        const params = ReqParams.parse_rsa(_this.key_mg, req, res);


        const reqfpw_aes = new AES(AES.derive_key(req.fingerprint_weak, req.salt, 1000));
        const reqfp_aes = new AES(AES.derive_key(req.fingerprint, req.salt, 1000));
        const key_aes = new AES(params.key);

        const cookie_aes = new AES();
        const cookie_aes_enc = key_aes.encrypt(cookie_aes.key)

        const target_session_table = flyauth.table("sessions")//flyauth.flycms ? flyauth.flycms.get_model(`${_this.table_prefix}_sessions`) : _this.sessions_table;
        const target_tabs_table =  flyauth.table("session_tabs")//flyauth.flycms ? flyauth.flycms.get_model(`${_this.table_prefix}_session_tabs`) : _this.sessions_table;

        const sess = await Session.create(target_session_table, target_tabs_table, cookie_aes_enc, reqfp_aes.encrypt(params.key));
//        const sess = await Session.create(_this.sessions_table, _this.session_tabs_table, cookie_aes_enc, reqfp_aes.encrypt(params.key));

        const xss_encrypted = cookie_aes.encrypt(sess.xss_secret);
        delete sess.xss_secret;

        const enc_aes = new AES();
        sess.oaes = enc_aes.key;
        sess.cks = req.new_cks;

        const enc_keys = JSON.stringify({
          iaes: sess.oaes,
          oaes: params.secret
        });

        res.writeHead(200, {
          "Set-Cookie": [
            `${flyauth.cfg.auth_prefix}flyauth_client_key=${encodeURIComponent(flyauth.key_mg.encrypt(req.new_client_key))}; Path=/; SameSite=Strict; Secure; HttpOnly`,
            `${flyauth.cfg.auth_prefix}flyauth_enc_key=${encodeURIComponent(flyauth.key_mg.encrypt(reqfpw_aes.encrypt(enc_keys)))}; Path=/; SameSite=Strict; Secure; HttpOnly`,
            `${flyauth.cfg.auth_prefix}flyauth_anti_xss=${encodeURIComponent(flyauth.key_mg.encrypt(xss_encrypted))}; Path=/; SameSite=Strict; Secure; HttpOnly`
          ],
          "Content-Type": `text/plain`
        });

        const secret_aes = new AES(params.secret);
        const data_encrypted = secret_aes.encrypt(JSON.stringify(sess));
        res.end(data_encrypted);

      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });


    this.http.post(flyauth.cfg.web_route_path+"/flyauth/signin", ...flyauth.default_middlewares, async (req, res) => {
      try {
        const params = ReqParams.parse_aes(req, flyauth.cfg.auth_prefix);
        if (params &&
          params.username &&
          params.username.match(/^[A-Za-z0-9_.-]+$/) &&
          params.password &&
          params.password.length > 4 &&
          params.fingerprint &&
          params.tab_id &&
          params.csrf_secret &&
          params.csrf_secret.length === 32
        ) { 
          const target_accounts_table = flyauth.table("accounts");
          const target_session_table = flyauth.table("sessions");
          const target_tabs_table =  flyauth.table("session_tabs");
          const target_attempts_table =  flyauth.table("failed_attempts");

          const found = (await target_accounts_table.select(
            [ "id", "password" ], "username = $1", [ params.username ]
          ))[0];


          const session_id = params.session_id = (await target_tabs_table.select("session_id", "id = $1", [ params.tab_id ]))[0].session_id;

          if (found) {

            const session = (await target_session_table.select(["key", "cookie_secret"], "id = $1", [ session_id ]))[0];

            let auth_attempts = (await flyauth.pg.query(`SELECT * FROM ${target_attempts_table.name} WHERE userid = '${found.id}'`));
            if (auth_attempts.rows.length > 0) {
              auth_attempts = auth_attempts.rows[0];
            } else {
              auth_attempts = undefined;
            }

            if (
              found && bcrypt.compareSync(params.password, found.password) &&
              (
                (
                  auth_attempts &&
                  auth_attempts.captcha &&
                  bcrypt.compareSync(params.captcha, auth_attempts.captcha)
                ) || !auth_attempts || !auth_attempts.captcha
              )
            ) {

              if (auth_attempts && auth_attempts.captcha) {
                await flyauth.pg.query(`UPDATE ${target_attempts_table.name} SET times = 0, captcha = NULL WHERE userid = '${found.id}'`);
              }

              const fp_aes = new AES(AES.derive_key(req.fingerprint, req.salt, 1000));


              let salt = bcrypt.genSaltSync(10);

              const src_ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;

              salt = bcrypt.genSaltSync(10);
              const salted_src_ip = bcrypt.hashSync(src_ip, salt);

              salt = bcrypt.genSaltSync(10);
              const salted_fingerprint = bcrypt.hashSync(params.fingerprint, salt);

              const session_set_cmd = [
                'userid = $1',
                'src_ip = $2',
                'fingerprint = $3',
                'exp = $4'
              ];
              const session_set_val = [
                found.id,
                salted_src_ip,
                salted_fingerprint,
                Date.now() + _this.session_expiration
              ];



              const key_aes = new AES(fp_aes.decrypt(session.key));
              const cookie_secret = key_aes.decrypt(session.cookie_secret);
              const cookie_aes = new AES(cookie_secret);


              const xss_secret = cookie_aes.decrypt(
                flyauth.key_mg.decrypt(
                  decodeURIComponent(
                    req.cookies[flyauth.cfg.auth_prefix+"flyauth_anti_xss"]
                  ), "aes"
                )
              );

              const new_secrets = await Session.update(
                target_session_table,
                target_tabs_table,
                session_set_cmd,
                session_set_val,
                session_id,
                params.tab_id,
                params.csrf_secret,
                xss_secret,
                1 //ammount of ots to generate
              );

   
              const encrypted_response = req.oaes.encrypt(JSON.stringify({
                csrf_secrets: new_secrets.csrf
              }));

              res.writeHead(200, {
                "Content-Type": `text/plain`
              });

              res.end(encrypted_response);
            } else {
              const ip_addr = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
              if (auth_attempts) {
                if (auth_attempts.ip_addrs.includes(ip_addr)) {
                  await flyauth.pg.query(`UPDATE ${target_attempts_table.name} SET times = times + 1 WHERE userid = '${found.id}'`);
                } else {
                  await flyauth.pg.query(`UPDATE ${target_attempts_table.name} SET times = times + 1, ip_addrs = array_append(ip_addrs, '${ip_addr}') WHERE userid = '${found.id}'`);
                }
              } else {
                await flyauth.pg.query(`INSERT INTO ${target_attempts_table.name} ("userid", "times", "ip_addrs") VALUES ('${found.id}', 1, ARRAY['${ip_addr}'])`);
              }

              if (auth_attempts && auth_attempts.times >= flyauth.cfg.max_failed_attempts) {
                const captcha = CAPTCHA.generate(200, 80, 6);
                const salt = bcrypt.genSaltSync(10);
                const captcha_hash = bcrypt.hashSync(captcha.text, salt);
                await flyauth.pg.query(`UPDATE ${target_attempts_table.name} SET captcha = '${captcha_hash}' WHERE userid = '${found.id}'`);
                res.writeHead(403, {
                  'Content-Type': 'image/png',
                  'Content-Length': captcha.buffer.length,
                  "Set-Cookie": await Session.end(
                    target_session_table,
                    target_tabs_table,
                    params,
                    flyauth.cfg
                  )
                });
                res.end(captcha.buffer);
              } else {
                await _this.delay("bad_pwd");
                res.writeHead(401, {
                  "Content-Type": "text/plain",
                  "Set-Cookie": await Session.end(
                    target_session_table,
                    target_tabs_table,
                    params,
                    flyauth.cfg
                  )
                });
                res.end("Unauthorized");
              }
            }

          } else {
            await _this.delay("no_user");
            res.writeHead(401, {
              "Content-Type": "text/plain",
              "Set-Cookie": await Session.end(
                target_session_table,
                target_tabs_table,
                params,
                flyauth.cfg
              )
            });
            res.end("Unauthorized");
          }
        } else {
          res.writeHead(422, {"Content-Type": "text/plain"});
          res.end("Unprocessable Entity");
        }
      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });


    this.http.post(flyauth.cfg.web_route_path+"/flyauth/signout", ...flyauth.default_middlewares, flyauth.authorize_mwfn, async (req, res) => {
      try {
        
        const params = ReqParams.parse_aes(req, flyauth.cfg.auth_prefix);

        res.writeHead(200, {
          "Content-Type": "text/plain",
          "Set-Cookie": await Session.end(
            this.sessions_table,
            this.session_tabs_table,
            params,
            flyauth.cfg
          )
        });
        res.end("OK");

      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });


    this.http.post(flyauth.cfg.web_route_path+"/flyauth/last-ots", ...flyauth.default_middlewares, flyauth.authorize_mwfn, async (req, res) => {
      try {
        
        const params = ReqParams.parse_aes(req, flyauth.cfg.auth_prefix);


        res.writeHead(200, {
          "Content-Type": `text/plain`
        });
        res.end(req.oaes.encrypt(req.flyauth.ncsrfs));

      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });

    this.http.get(flyauth.cfg.web_route_path+"/flyauth/public-key", async (req, res) => {
      try {
        res.writeHead(200, {
          "Content-Type": `text/plain`
        });
        res.end(_this.key_mg.latest.rsa.public_key);
      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Internal Server Error");
      }
    });
  }

  async delay(delay_type) {
    const delay_ms = Math.round(
      (this.cfg.auth_delay[delay_type].max - this.cfg.auth_delay[delay_type].min) * Math.random() + this.cfg.auth_delay[delay_type].min
    );
    console.log("delay", delay_type, delay_ms);
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, delay_ms);
    });
  }
}
