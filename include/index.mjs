
import fs from 'fs'
import path from 'path'

import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

export default class IncludeScripts {

  static head(cfg, pub_key, enc_creds) {
    if (!enc_creds) {
      enc_creds = "undefined";
    } else {
      enc_creds = `'${enc_creds}'`;
    }
    let script_src = fs.readFileSync(path.resolve(__dirname, "head/main.js"), "utf8");
    script_src = `
      const AUTHING_PREFIX = "${cfg.auth_prefix}";
      const AUTHING_PATH = "${cfg.web_route_path}";
      const AUTHING_PUBLIC_KEY = \`${pub_key}\`;
      const AUTHING_CREDENTIALS = {
        enc: ${enc_creds}
      };
      const AUTHING_OBSERVER_CONFIG = { attributes: true, childList: true, subtree: true };
      let AUTHING_OBSERVER = undefined;
      ` + script_src;
    return script_src;
  }


  static body() {
    return fs.readFileSync(path.resolve(__dirname, "body/main.js"), "utf8");
  }
}
