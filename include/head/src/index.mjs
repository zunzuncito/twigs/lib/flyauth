
import URLUtil from 'zunzun/flyutil/url.mjs'

import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

import bcrypto from 'zunzun/flypto/bcrypt.mjs'

let cur_ctxmenu_e = undefined;

const fp_base = 1.5;

const ctx_menu_closed = async (e) => {
  if (cur_ctxmenu_e) {
    const dev_fp = Fingerprint.get(fp_base);
    const fp_salt = Fingerprint.get_salt();
    console.log("LOADED", dev_fp, fp_salt);
    const cred_parser = new CredParser(dev_fp, fp_salt);
    const creds = await cred_parser.next();
    cur_ctxmenu_e.href = cur_ctxmenu_e.ctxm_flyauth_href + (creds ? `?cks=${creds.cks}&authorized=${creds.authorized}` :  '')
    cur_ctxmenu_e = undefined;
  }
}

const append_newtab = (tab_url) => {
  return tab_url.includes("?") ? "&new_tab=true" : "?new_tab=true";
}

window.addEventListener("blur", ctx_menu_closed);

window.addEventListener("load", (e) => {
  document.body.addEventListener("mouseup", ctx_menu_closed);
});

const parse_url = (url_to_parse) => {
  if (url_to_parse.includes("?")) {
    const url_split = url_to_parse.split("?");
    return {
      parsed_url: url_split[0],
      parsed_params: url_split[1]
    }
  } else {
    return {
      parsed_url: url_to_parse,
      parsed_params: ""
    }
  }
}

(() => {
  const dev_fp = Fingerprint.get(fp_base);
  const fp_salt = Fingerprint.get_salt();
  console.log("LOADED", dev_fp, fp_salt);
  const cred_parser = AUTHING_CREDENTIALS.parser = new CredParser(dev_fp, fp_salt);
  
  console.log("CRED PARSER DONE CONSTRUCTIN");

  (async () => {
    try {
      const creds = await cred_parser.next();
      if (creds) window.history.pushState('', '', window.location.toString().replace(/authorized=.+?(?=&|$)/, `authorized=${creds.authorized}`));
    } catch (e) {
      console.error(e);
    }
  })()

  const authorize_url = async (iurl) => {
    const { parsed_url, parsed_params } = parse_url(iurl);
    const credsrpl = await cred_parser.next();
    const ourl = parsed_url + (
      credsrpl ? (
        parsed_params !== "" ?
          `?${parsed_params}&cks=${credsrpl.cks}&authorized=${credsrpl.authorized}` :
          `?cks=${credsrpl.cks}&authorized=${credsrpl.authorized}`
      ) : (
        parsed_params !== "" ? `?${parsed_params}` : ''
      )
    );
    return ourl;
  }

  let external_resources = [];

  const handle_external = (element) => {
    external_resources.push(element);
    element.addEventListener("load", (evt) => {
      element.loaded = true;
    });
    element.addEventListener("loadeddata", (evt) => {
      element.loaded = true;
    });
    element.addEventListener("error", (evt) => {
      element.loaded = true;
    });
    element.addEventListener("canplay", function() {
      element.loaded = true;
    });
  }

  const flyload_event = new Event('flyload');
  window.addEventListener('load', (e) => {
    if (element_queue.length > 0 || external_resources.length > 0) {
      const flyload_interval = setInterval(() => {
        if (element_queue.length == 0) {
          let all_loaded = true;
          for (let er of external_resources) {
            if (!er.loaded) all_loaded = false;
          }

          if (all_loaded) {
            clearInterval(flyload_interval);
            external_resources = [];
            window.dispatchEvent(flyload_event);
          }
        }
      }, 100);
    } else {
      window.dispatchEvent(flyload_event);
    }
  });

  let element_queue = [];
  const handle_mutation = (element) => {
    const handling = element_queue.length > 0;
    element_queue.push(element);

    if (!handling) {
      handle_queue()
    }
  }

  const handle_queue = async () => {
    try {
    for (let element of element_queue) {
      const flyauth_src = element.getAttribute("flyauth_src");
      if (flyauth_src) {
        element.removeAttribute("flyauth_src");

        if (element.tagName.toLowerCase() == "script") {
          const nscript = document.createElement("script");
          handle_external(nscript);
          nscript.src = await authorize_url(flyauth_src);
          element.parentNode.replaceChild(nscript, element);
        } else if (element.tagName.toLowerCase() == "source") {
          const nvideo = document.createElement("source");
          handle_external(element.parentNode);
          nvideo.src = await authorize_url(flyauth_src);
          element.parentNode.replaceChild(nvideo, element)
          nvideo.parentNode.load();
        } else if (element.tagName.toLowerCase() == "img") {
          handle_external(element);
          element.src = await authorize_url(flyauth_src);
        } else {
          handle_external(element);
          element.src = await authorize_url(flyauth_src);
        }
      }

      const flyauth_href = element.getAttribute("flyauth_href");
      if (flyauth_href) {

        const authorize_new_tab = async () => {
          const ntab_href = append_newtab(element.href);
          element.href = await authorize_url(flyauth_href)
          window.open(ntab_href, '_blank');
        }

        element.addEventListener("contextmenu", (e) => {
          element.href += append_newtab(element.href);
          element.ctxm_flyauth_href = flyauth_href;
          element.context_menu_opened = true;
          cur_ctxmenu_e = element;
        });
        
        element.addEventListener("click", (e) => {
          e.preventDefault();
          if (e.ctrlKey || e.metaKey || e.shiftKey || e.which === 2) {
            authorize_new_tab();
          } else {
            window.location.href = element.href;
          }
        });
        element.addEventListener("auxclick", (e) => {
          e.preventDefault();
          authorize_new_tab();
        });

        element.href = await authorize_url(flyauth_href)
        element.removeAttribute("flyauth_href");
      }

      const flyauth_action = element.getAttribute("flyauth_action");
      element.removeAttribute("flyauth_action");
      const flyauth_method = element.getAttribute("flyauth_method");
      element.removeAttribute("flyauth_method");

      if (flyauth_action) {
        element.action = flyauth_action;

        const submit_listener = async (evt) => {
          evt.preventDefault(); 
          const enc_type = element.enctype || element.getAttribute("enctype");

          const formdata = new FormData(element);


          if (flyauth_method) {

            const formobj = {};
            for (const [key, value] of formdata) {
              formobj[key] = value;
            }

            const creds = await cred_parser.next(formobj);
            if (creds) {
              await fetch(element.action, {
                method: flyauth_method,
                body: `cks=${creds.cks}&authorized=${creds.authorized}` // formData should contain the file
              });
              window.location.reload();
            }
          } else if (enc_type == "multipart/form-data") {

            const creds = await cred_parser.next();

            if (creds) {
              await fetch(element.action, {
                method: 'POST',
                headers: {
                  'Authorization': `cks=${creds.cks}&authorized=${creds.authorized}`,
                },
                body: formdata, // formData should contain the file
              });
              window.location.reload();
            }
          } else {
            const formobj = {};
            for (const [key, value] of formdata) {
              formobj[key] = value;
            }

            const creds = await cred_parser.next(formobj);

            if (creds) {
              const tmp_form = document.createElement('form');

              tmp_form.action = element.action;
              tmp_form.method = element.method;

              const tmp_form_cks = document.createElement('input');
              tmp_form_cks.type = "hidden";
              tmp_form_cks.name = "cks";
              tmp_form_cks.value = creds.cks;
              tmp_form.appendChild(tmp_form_cks);

              const tmp_form_authorized = document.createElement('input');
              tmp_form_authorized.type = "hidden";
              tmp_form_authorized.name = "authorized";
              tmp_form_authorized.value = creds.authorized;
              tmp_form.appendChild(tmp_form_authorized);

              element.parentNode.replaceChild(tmp_form, element);


              tmp_form.submit();
            } else {
              element.removeEventListener("submit", submit_listener);
              element.submit();
            }
          }
        }
        if (element.current_listener) element.removeEventListener("submit", element.current_listener);
        element.current_listener = submit_listener;
        element.addEventListener("submit", submit_listener);
      }
    }
    element_queue = [];
  } catch (e) {
    console.error(e);
  }
  }
  
  const flyauth_observer_callback = async function(mutationsList, observer) {
    try {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList') {
          for (let n = 0; n < mutation.addedNodes.length; n++) {
            if (mutation.addedNodes[n].nodeType === 1) { // Element node (1)
              handle_mutation(mutation.addedNodes[n]);
            }
          }
        }
      }
      return;
    } catch (e) {
      console.error(e);
    }
  };

  // Create an observer instance linked to the callback function
  AUTHING_OBSERVER = new MutationObserver(flyauth_observer_callback);
  AUTHING_OBSERVER.observe(document.getElementsByTagName('head')[0], AUTHING_OBSERVER_CONFIG)
})();
