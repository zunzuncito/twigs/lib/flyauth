
import crypto from 'crypto'

import RSA from "zunzun/flypto/rsa.mjs"
import AES from "zunzun/flypto/aes.mjs"

export default class KeyManager {
  constructor(ctx_group, prefix) {
    this.passphrase = crypto.randomBytes(64).toString("base64");
    this.ctx_group = ctx_group;

    this.prefix = prefix;

    this.update();
  }

  update() {
    this.keys = {
      rsa: [
        new RSA(1024, this.passphrase)
      ],
      aes: [
        new AES()
      ]
    }
    this.latest = {
      rsa: this.keys.rsa[this.keys.rsa.length-1],
      aes: this.keys.aes[this.keys.aes.length-1]
    }
    this.ctx_group.g_tpl_ctx[this.prefix+'flyauth_public_key'] = this.latest.rsa.public_key;
  }

  encrypt(data) {
    return this.latest.aes.encrypt(data);
  }

  decrypt(enc_data, algorithm) {
    if (!algorithm) algorithm = "rsa";
    return this.latest[algorithm].decrypt(enc_data, this.passphrase);
  }
}
