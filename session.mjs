
import crypto from 'crypto'
import bcrypt from 'bcrypt'

import AES from 'zunzun/flypto/aes.mjs'

function hash_csrf(csrf_buffer, blocks, block_length) {
  let hash_buff = Buffer.alloc(block_length, 0);
  for (let b = 0; b < blocks; b++) {
    for (let i = 0; i < block_length; i++) {
      hash_buff[i] += csrf_buffer[block_length*b+i];
    }
  }
  return hash_buff;
}


export default class Session {
  static async create(sessions_table, session_tabs_table, cookie_aes_enc, key) {
    try {
      const csrf_secret = crypto.randomBytes(24);
      const xss_secret = crypto.randomBytes(24);

      const session_id = (await sessions_table.insert(
        ['cookie_secret', 'key'], 
        [
          '$1',
          '$2'
        ],
        [cookie_aes_enc, key], 'RETURNING ID'
      )).id;
      const tab_id = (await session_tabs_table.insert(
        ['session_id', 'csrf_secrets', 'xss_secret'], ['$1', '$2', '$3'], [
          session_id,
          [
            hash_csrf(csrf_secret, 4, 6).toString("base64"),
          ],
          hash_csrf(xss_secret, 4, 6).toString("base64")
        ], 'RETURNING ID'
      )).id;

      return {
        csrf_secret: csrf_secret.toString("base64"),
        xss_secret: xss_secret.toString("base64"),
        tab_id: tab_id
      }
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static async update(
    sessions_table,
    session_tabs_table,
    set_cmd,
    set_val,
    session_id,
    tab_id,
    csrf,
    xss,
    gen_csrf,
    overwrite_csrf,
    new_tab
  ) {
    try {
      const session_secrets = (await session_tabs_table.select(['csrf_secrets', 'xss_secret'], 'id = $1', [tab_id]))[0];
      let csrf_hash_match = undefined;

      for (let csrf_hash of session_secrets.csrf_secrets) {
        if (hash_csrf(Buffer.from(csrf, 'base64'), 4, 6).toString("base64") === csrf_hash) {
          csrf_hash_match = csrf_hash;
          break;
        }
      }

      let xss_hash_match = undefined;
      if (hash_csrf(Buffer.from(xss, 'base64'), 4, 6).toString("base64") === session_secrets.xss_secret) {
        xss_hash_match = session_secrets.xss_secret;
      }

      if (csrf_hash_match && xss_hash_match) {

        await session_tabs_table.update([
          `csrf_secrets = array_remove(csrf_secrets, $1)`,
        ], 'id = $2', [
          csrf_hash_match,
          tab_id
        ]);

        if (set_cmd.length > 0) {
          const id_index = set_val.length + 1;
          set_val.push(session_id);
          await sessions_table.update(set_cmd, `id = $${id_index}`, set_val);
        }


        if (new_tab) {
          const csrf_secret = crypto.randomBytes(24);
          const xss_secret = crypto.randomBytes(24);

          tab_id = (await session_tabs_table.insert(
            ['session_id', 'csrf_secrets', 'xss_secret'], ['$1', '$2', '$3'], [
              session_id,
              [
                hash_csrf(csrf_secret, 4, 6).toString("base64"),
              ],
              xss_hash_match
            ], 'RETURNING ID'
          )).id;
        }


        if (typeof gen_csrf === 'number' && gen_csrf > 0) {
          const new_csrf = [];
          const new_csrf_db = [];
          for (let i = 0; i < gen_csrf; i++) {
            const ncsrf = crypto.randomBytes(24);
            new_csrf.push(ncsrf.toString("base64"));
            new_csrf_db.push(hash_csrf(ncsrf, 4, 6).toString("base64"));
          }
          const csrf_set = overwrite_csrf ? `csrf_secrets = $1` : `csrf_secrets = array_cat(csrf_secrets, $1)`;
          await session_tabs_table.update([
            csrf_set,
          ], `id = $2`, [
            new_csrf_db,
            tab_id
          ]);
          return {
            csrf: new_csrf,
            tab_id: tab_id
          };
        } else {
          return {
            tab_id: tab_id
          };
        }
      } else {
        return undefined;
      }
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static async end(
    sessions_table,
    session_tabs_table,
    params,
    cfg
  ) {
    try {
      const session_id = (await session_tabs_table.select(['session_id'], "id = $1", [params.tab_id]))[0].session_id;
      await session_tabs_table.delete("session_id = $1", [ session_id ]);
      await sessions_table.delete("id = $1", [ session_id ]);
      return [
        `${cfg.auth_prefix}flyauth_client_key=; Max-Age=0; Path=/; HttpOnly`,
        `${cfg.auth_prefix}flyauth_enc_key=; Max-Age=0; Path=/; HttpOnly`,
        `${cfg.auth_prefix}flyauth_anti_xss=; Max-Age=0; Path=/; HttpOnly`
      ];
    } catch (e) {
      console.error(e.stack);
    }
  }
}
